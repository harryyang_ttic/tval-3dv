name='000060_clean.txt.png';
name2='000060_clean2.txt.png';
im=imread(name);

[H,W,~]=size(im);

im2=im;
radius=3;
for h=1:H
    for w=1:W
        if ~(im(h,w,1)==255 && im(h,w,2)==255 && im(h,w,3)==255)
            for hh=h-radius:h+radius
                for ww=w-radius:w+radius
                    if hh<1 || hh>H || w<1 || ww>W
                        continue;
                    end
                    dist=sqrt((hh-h)*(hh-h)+(ww-w)*(ww-w));
                    if dist<3
                        im2(hh,ww,:)=im(h,w,:);
                    end
                end
            end
        end
    end
end

imwrite(im2,name2);