name='outlier2.png';
name2='../img/000089.png';
name3='000089.png';
im=imread(name);
im2=imread(name2);

[H,W,~]=size(im);
im3=im2;

for h=1:H
    for w=1:W
        if ~(im(h,w,1)==255 && im(h,w,2)==255 && im(h,w,3)==255)
            im3(h,w,:)=im(h,w,:);
        end
    end
end

imwrite(im3,name3);