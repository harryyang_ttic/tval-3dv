\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{3dv}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{array}
\usepackage{graphicx}
%\usepackage{subfig}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multirow}% http://ctan.org/pkg/multirow
\usepackage{hhline}% http://ctan.org/pkg/hhline
%\usepackage{hyperref}


% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}


%\threedvfinalcopy % *** Uncomment this line for the final submission

\def\threedvPaperID{****} % *** Enter the 3DV Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifthreedvfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{\LaTeX\ Direct Lidar Evaluation of 3D Vision Algorithms}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
% \thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   We present a novel benchmark suite Tval that defines a new performance measure --- lidar prediction --- to evaluate 3D vision algorithms. While ground truth stereo and optical flow labelings are usually difficult to obtain, lidar data are becoming easy to acquire. We show that by integrating with other components, lidar can be used to quantitatively evaluate stereo and optical flow algorithms when ground truth labelings are unavailable. The benchmark allows us to validate the performance of vision algorithms in dynamic scenes. It should also make it possible to train visual systems directly from lidar. We have constructed an online evaluation system which includes an image-lidar dataset and a development kit to assist lidar prediction and scoring. Experiments show that our benchmark measures the performance of 3D vision systems accurately, and is particularly useful to evaluate flow algorithms when moving objects are present.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
We are motivated by the need for in-the-wild quantitative evaluation of 3D vision
algorithms such as stereo, optical flow and scene flow \cite{fritsch2013new, scharstein2014high}. In-the-wild evaluation is non-trivial
since ground truth is usually difficult to obtain and subject to errors. As a consequence, optical flow is typically
evaluated using synthetic dataset such as the Yosemite sequence \cite{austvoll2005study} and more recently
the MPI Sintel dataset \cite{butler2012naturalistic}. Difficulties in obtaining ground truth limited the contents of KITTI dataset \cite{Geiger2013IJRR, geiger2012we} --- in order to improve ground truth density the disparity at each frame is constructed using data augmented over several sweeps of lidar scan, and the environment must not change during the scan. This excludes moving objects such as cars or pedestrians from the dataset. In reality, however, the static scenes assumption could be easily violated, especially in urban areas where the components are constantly changing.

\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{img/fig0/fig0c.pdf}
\caption{Direct lidar evaluation of stereo. Top left: one image of a stereo pair given as input to the stereo algorithm. Top right: the disparity map computed with the stereo algorithm. Bottom left: reconstructed 3D scene geometry with triangular mesh. Bottom right: the result of lidar prediction acquired by intersecting each lidar ray with the 3D shapes. Different colors encode different accuracy --- the red points are where the lidar predictions have a large discrepancy comparing with the ground truth.}
\label{fig:kitti_error}
\end{figure} 

To address this issue, we define a new performance measure --- lidar prediction --- as a way to evaluate 3D vision system. We observe that with the advancing of lidar techonology, it has become relatively easy to acquire large amount of lidar data, such as in the case of KITTI. On the other hand, if we could have a geometry representation of the scene, we could predict the distance of each ray by intersecting virtual lidar rays with the shapes. For the vision algorithms that do not directly produce 3D scene geometry such as stereo or optical flow, we can easily combine their output with other modules and evaluate the final 3D reconstruction. By carefully designing auxiliary modules, the evaluation of 3D reconstruction is a reliable measurement of the accuracy of the underlying stereo or optical flow algorithms. An immediate reward of using lidar is that it avoids the hassles of inferring dense ground truth at the pixel level. Note that at each frame the semi-dense ground truth point cloud is produced by one rapid cycle of lidar spinning, so that object movements in the scene could be tolerated. This allows us to collect a more dynamic dataset with richer content.

\noindent\textbf{Tasks and Dataset.} We define three tasks based on such measure --- a stereo task, a 100ms flow task and a 300ms flow task.
For the stereo task, the
input consists of a single stereo pair and a set of lidar rays sent approximately at the same time when the images are taken.  For the 100ms and 300ms flow tasks, the input
consists of two stereo pairs taken 100 milliseconds apart and a set of lidar rays sent at
approximately 100 and 300 milliseconds {\em after} the second frame was taken.  In both cases the task is to predict the measured distance for each ray given its direction.  Each lidar ray can be represented with a unit vector, telling its direction and a scalar, telling the distance of its endpoint. The tasks are then to predict the distance of the endpoint given the unit direction vector. Such prediction would need to rely on depth or flow estimation as well as scene reconstruction.

We published our dataset along with the online evaluation server. The stereo dataset contains 875 samples of image-lidar frames, and the flow dataset contains 195 samples of image-lidar sequences.  Each flow sequence contains five consecutive frames of image-lidar recorded at a speed of 10Hz (note that only the first two stereo pairs and the lidar scan at the third frame and fifth frame are used). We randomly split the dataset into the training set and the test set. The training set and test set for stereo has 584 and 291 samples, respectively, and the for optical flow it has 133 and 65 samples, respectively. In the test set, the distance of each lidar ray is not given --- one must predict the distance and submit it to the server. 

The resolution of stereo images and the density of lidar points are described in Fig.~\ref{fig:intro}.

\begin{figure} %\left
\includegraphics[width=0.5\textwidth]{img/fig1/image1_top3.pdf}
\caption{Visualization of a stereo pair and corresponding lidar file. \textbf{Top:} the stereo pair. Images are monochrome and have resolution of $1600 \times 848$. The zoom-in shows the viewpoint difference between the left and right camera. \textbf{Bottom:} visualization of the lidar file. The lidar takes a full panoramic scan of 145, 927 lidar points. Zoom in shows the lidar points of the moving cars.}
\label{fig:intro}
\end{figure}

\noindent\textbf{Development Kit.} To assist evaluation of stereo and optical flow algorithms, the online suite comes with a development kit providing default building blocks for 3D reconstruction and lidar prediction on the basis of stereo/flow estimation. The registration between lidar and camera is also an essential knowledge for lidar prediction~\cite{geiger2012automatic, levinson2013automtic} . Default values of camera-lidar registration and camera calibration are provided, which are measured on vehicle hardware and then optimized with our own performance measure. 

In spite we have carefully selected auxiliary reconstruction functions to make the process robust to underlying core algorithms to evaluate, users are encouraged to optimize on every step of the reconstruction. The ultimate goal is to evaluate the reconstruction of the scene, as we believe knowing the exact geometry of the scene is crucial to applications such as autonomous driving.

\noindent\textbf{Relationship with Autonomous Learning.} A potential application of Tval's sensor-prediction tasks is for autonomous learning.
By ``autonomous learning'' we mean learning from interaction with the world without the use of
manual labeling.  Our performance measure is defined entirely in terms of predicting distance
measurements for lidar rays, so that learning of visual systems can be done on raw lidar data by optimizing this performance. We are particularly interested in the possibility of training neural networks in an unsupervised fashion, by optimizing performance on the Tval
tasks. We note that the current leading system on the KITTI stereo leader board uses a DNN disparity
match energy~\cite{vzbontar2014computing}.

\section{Notation and Scoring}

We will use $R_i$ to denote the $i$th lidar ray defined by a direction vector (in lidar coordinates) where the origin point is located at the position of the lidar.
We let $D_i$ denote the measured distance for liday ray $R_i$. We let $X_i$ be the point in space (in lidar coordinates) that lies
at distance $D_i$ from the given origin point of the ray $R_i$ along the direction specified by ray $R_i$.  The set of points $X_i$
is the normal point cloud determined by the lidar unit.

To define the performance metric, we consider a lidar ray $R$ with measured distance $D$ and predicted distance $\widehat{D}$ where, for definiteness,
both $D$ and $\widehat{D}$ are measured in meters.
We define the ``pixel error'' of this prediction to be
\begin{equation}\label{eq:threshold}
\mathbf{err}(\widehat{D},D) = \frac{|\widehat{D}-D|fb}{D\widehat{D}}
\end{equation}
where $f$ is the focal length of the cameras in pixels and b is the baseline separation, in meters, between the stereo cameras.
Note that $\mathbf{err}(\widehat{D},D)$ has units of pixels --- the distance unit cancels.
This error measure roughly corresponds to pixel error in the disparity map for the stereo task.  To see this we note the following
where $Z$ and $\widehat{Z}$ are the $z$-coordinates of the true and predicted points in camera coordinates, and $d$ and $\hat{d}$ are the true
and estimated disparities in pixels:
\begin{eqnarray*}
Z & \approx & D \\
\\
\widehat{Z} & \approx & \widehat{D} \\
\\
d & = & \frac{fb}{Z} \; \approx \; \frac{fb}{D} \\
\\
\hat{d} & = & \frac{fb}{\widehat{Z}} \;\approx \; \frac{fb}{\widehat{D}}
\end{eqnarray*}
We then get $\mathbf{err}(\widehat{D},D) \approx |\hat{d} - d|$.
The $k$-pixel error rate is then the fraction of rays $R_i$ for which $\mathbf{err}(\widehat{D}_i,D_i) > k$. The default setting of $k$ in our evaluation is 3, which is comparable to the metric used by KITTI.

It is important to note that the score is defined independently of any coordinate transformation matrix relating camera coordinates to lidar coordinates.
This ensures that there is no chance for the results to be biased by a choice of calibration method
for the coordinate transformation.
More importantly, it allows coordinate transformation to be learned by score optimization on the training data without the transformation
matrix interfering with the scoring.

\noindent\textbf{Outlier Rays.}
The Tval score is computed from an ``inlier subset'' of the lidar rays actually sent by the lidar
system. We use the outlier identification system provided by the Velodyne manufacturer.
More specifically, for each lidar ray $R_i$, the system computes the mean distance
$\epsilon_i=\frac{1}{k}\sum\limits_{j=1}^k \epsilon_{ij}$ from the point $X_i$ to its $k$-nearest neighbors, where
$\epsilon_{ij}=d(X_i,X_j)$. It then fits a Gaussian distribution to the scalar values $\epsilon_i$ for all $i$.  Let $\mu$ and $\sigma$ be
the mean and standard deviation of the Gaussian fit to the values $\epsilon_i$.
The outlier subset is defined to be the rays $R_i$ such that $|\epsilon_i - \mu| > 1.96\cdot\sigma$ ($\epsilon_i$ lies outside of the 95\% confidence interval for the Gaussian).
Outlier rays are not used in either the training or test data for the Tval tasks.

\noindent\textbf{Easy/Hard Rays Classification}
Inlier rays --- rays not removed as outliers --- are further classified into ``easy'' and ``hard'' rays.
The Tval leaderboard reports error rates on ``easy'' rays and entire set of rays (``easy''+``hard'') separately. The ``hard'' rays are defined to be the rays with their endpoints lying on edges of geometry shapes. We used 3D boundary detection algorithm to detect depth discontinuities and high curvature regions, where the endpoints are labeled as belonging to ``hard'' rays.  

\section{Evaluation Methodology}

This section describes our development kit and how the provided tools can be used to combine with stereo or optical flow algorithms to produce meaningful lidar predictions.

\subsection{The Development Kit}
Our performance measure is based upon the ability of predicting lidar distance, which requires estimating the 3D geometry of the scene. The construction of 3D geometry can be broken into components and, in particular, our development kit provides default tools for building auxiliary components. This allows the users to focus more on the development of the core visual system such as stereo and optical flow.

\noindent\textbf{The 3D Reconstruction Tool.} We provide the slanted plane converter, which performs slanted-plane smoothing of a point cloud using a CRF \cite{yamaguchi2014efficient}. The mesh converter, on the other hand, uses Delaunay triangulation to construct a triangle mesh from the point cloud. Both tools produce a set of finite-region 3D planes where each plane corresponds to an image segment and encodes its depth information.

\noindent\textbf{Ego-motion Estimator Tool.} The tool estimates the ego-motion of the camera, by noting the relation between translational component of the flow vector at point $p$ and camera motion $v_z$~\cite{yamaguchi2013robust}:
\begin{eqnarray}
d(p,Z_p)=|p+u_w(p)-o'|\frac{v_z}{Z_p-v_z}.
\end{eqnarray}
Here $Z_p$ is the depth of the pixel, $u_w(p)$ is the flow component due to camera rotation, and $o'$ is the epipole after applying rotation. Given the estimation of flow $d$ and depth $Z_p$, we can estimate the camera translation $v_z$ by minimizing the difference. Since estimating the ego-motion of the camera assumes the scene is static and the camera motion is rigid, its performance will be deteriorated when moving objects are present. 

\noindent\textbf{Ray Casting Tool.} The tool that intersects lidar rays with 3D planes is also included in the development kit. Given a particular estimate of lidar-camera geometry, the flight time of a lidar ray is predicted by linearly tracing a ray in space as a function of time. More rigorously speaking, given the lidar origin position $P_o$ in camera frame and the directional vector $\vec{v}$, the position of the lidar ray at flight time $t$ can be expressed as $P_t=P_o+ct\vec{v}$ and the corresponding position on image plane is given by $p_t=M_{proj}P_t$ where $M_{proj}$ is the camera projection matrix. The depth at $p_t$ is given by $d(p_t,\theta_j)$, where $\theta_j$ is the parameterization of the 3D plane corresponding to the segment containing $p_t$. The ray tracing terminates when $d(p_t,\theta_j)$ indicates that $P_t$ traverses behind the 3D plane. 

The occlusion situation can be easily handled in the case. The lidar ray is assigned to one of the segments of the image --- either the visible segment $j$ ``hit'' by the ray \textit{or} the segment that the ray passed over before being ``occluded'' by a neighboring segment. This is based on whether the ray-plane intersection projects inside or outside the segment in image (Fig.~\ref{fig:occlusion}). Once the ray has been assigned to a segment, the lidar distance can be predicted by solving for the point in space where the ray intersects the plane defined by the associated segment.

\begin{figure}
\centering
%\begin{subfigure}[b]{0.45\textwidth}
 				\includegraphics[width=0.45\textwidth]{img/img4/occlusion7.pdf}
%                \label{fig:occlude}
%\end{subfigure}
\caption{The lidar ray tracing model. \textbf{Left:} the lidar ray terminates at point $P$ where it hits the slanted disparity plane $p_2$ occluded by $p_1$. The ray is then assigned to plane $p_2$. \textbf{Right:} the corresponding trajectory of the lidar ray on image, where the ray hits the car in the back (invisible to camera). Each segment on image represents a slanted disparity plane.}
\label{fig:occlusion}
\end{figure}

\noindent\textbf{Calibration of Camera-Lidar Geometry.}
As part of the converter, we describe our method to register between camera and lidar using the training data. Initialized with hardware measurement, the calibration is then based on the result of SPS-St\cite{yamaguchi2014efficient} and the the slanted plane stereo converter. We iteratively update the transform between camera and lidar to minimize the 3-pixel error rate, based on rotation and translation estimation and shape registration~\cite{wheeler1995iterative}. Note that the assignment of lidar ray to image segment changes with camera-lidar geometry and needs to be updated at every iteration, similar to ICP~\cite{besl1992method}.

\noindent\textbf{Default Depth Estimation for Flow.}
In order to accommodate optical flow evaluations, we provide default depth estimation (disparity) for the image sequences of flow at time $t_1$ and $t_2$, which can be used as prior knowledge of initial scene geometry. The disparity is computed using SPS-St. 

\subsection{Predicting Lidar Distance From Stereo and Optical Flow}

There are multiple ways to predict the lidar distance from stereo and optical flow algorithms using default tools. Given stereo algorithms and the output disparity maps, one can use the 3D reconstruction tool --- either the slanted plane or the 3D mesh --- to create a set of 3D planes. Then the ray casting tools can be used to predict the lidar distance. For the optical flow, one can use the ego-motion estimator to estimate the ego motion of the camera, and in the meantime create a set of 3D planes using 3D reconstruction tool at initial time, based on default depth estimation. Then the planes can translate to time $t_3$ and $t_5$ and we can get lidar prediction using raycasting tool. The user can also estimates point motion directly, using the knowledge of depth for each pixel at time $t_1$ and $t_2$ and their correspondence given the flow, and we can move each point separately to get the point cloud at time $t_3$ and $t_5$. The methods quantitatively evaluated in Section 4.

\section{Data Collection}

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{img/trina_car_plain.pdf}
\caption{Test vehicle used for data collection.  The Velodyne HDL-64E Lidar and stereo camera setup are mounted on the roof rack.  Details of the sensors are in the text.}
\label{fig:test_vehicle}
\end{figure}

The test vehicle has a similar hardware setup to many other autonomous
vehicle projects \cite{Levinson-RSS-07, urmson2008autonomous, Geiger2013IJRR}.
The pose and motion estimates come from an Applanix POS-LV 220 inertial GPS 
navigation system.  This system generates pose estimates at 100 Hz.  The lidar data
is from a Velodyne HDL-64E, which uses 64 laser beams at fixed vertical angles, and spins at 10
Hz.  Each measured point is computed using the vertical angle, horizontal angle, range, and an intensity
value.  The 3D position is computed using the vertical angle, horizontal angle, and range.  The vertical angle
is a fixed measured value, the horizontal angle is measured using a 36,000 tick encoder, and the range
is measured using a resolution of 5cm.  In addition, the Velodyne company has developed custom
calibration and position estimation algorithms for estimating 3D point position, see \cite{Velodyne_manual}
for more details.
The intensity is an 8 bit value which is a measurement of the reflectivity of the surface being illuminated.  

The camera data is from a pair of Prosilica GE1660 Monochrome 
cameras, each with resolution $1600\times 1200$.  The initial camera calibration for both intrinsic and extrinsic transformation 
between cameras is measured in traditional checkerboard techniques.

Using these calibration parameters, we perform stereo rectification and
then trim bottom portion of the image.  We have designed a custom camera trigger device that monitors UDP packets 
from the velodyne and synchronizes camera firing when the cameras and Velodyne are aligned.  This eliminates inconsistencies 
due to time synchronization effects.  We use a hand-measured transformation matrix between one of the cameras and the 
Velodyne sensor as a starting point for our automatic calibration method.

For flow evaluation, to generate datasets of relatively independent video sequences (each containing three consecutive image frames), we use the output of our inertial GPS system. We record a new sequence of video when the vehicle has travelled more than 50 meters from the previous location, or has a yaw difference of more than 85 degrees.

\section{Results}

To demonstrate the effectiveness of our performance measure, we evaluated the state-of-art stereo and optical flow methods combining with different converters. Results are shown in Table~\ref{tb:stereo} and Table~\ref{tb:flow}.  

\begin{table*}[!ht]
\begin{center}
\resizebox{1\textwidth}{!}{%
  \begin{tabular}{ r | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm}  }
    \hline
    \multirow{2}{*}{Stereo} & \multicolumn{2}{c|}{Slanted Plane Converter} & \multicolumn{2}{c|}{Delauny Triangulation Converter} &   \multirow{2}{*}{KITTI-Noc} &   \multirow{2}{*}{KITTI-All} \\ \hhline{~----~~}
    &Easy Points & All Points & Easy Points & All Points &&\\
    \hline
    SPS-St~\cite{yamaguchi2014efficient} & 4.23\% & 5.79\% & 6.70\% & 7.73\% & 3.39\%  &  4.41\%  \\ 
    rSGM~\cite{spangenberg2014large} &5.71\%& 7.33\% & 8.02\% & 8.77\% & 5.03\% & 6.60\%\\
    opencv-BM~\cite{bradski2000opencv} &6.33\%& 7.88\% & 13.25\% & 15.39\% & 12.28\% & 13.76\% \\
    opencv-SGBM~\cite{hirschmuller2008stereo} &9.32\%& 10.32\% & 12.31\% & 13.01\% & 7.64\% &  9.13\%\\
    ELAS~\cite{geiger2011efficient} &10.56\%& 11.09\% & 12.73\% & 13.64\% &8.24\% & 9.96\%\\ 
    GCS~\cite{cech2007efficient} &23.09\%& 19.91\% & 26.65\% & 23.40\% & 13.38\% & 14.54\%\\
     S+GF~\cite{zhang2014cross} &29.94\%&34.82\% & 31.09\% & 37.09\% &11.21\%	& 11.21\% \\
     \hline
  \end{tabular}}
\end{center} 
\caption{3-pixel error rate of stereo algorithms using different conversions and their error rate in KITTI.}
\label{tb:stereo}
\end{table*}

\begin{table*}[!ht]
\begin{center}
\resizebox{1\textwidth}{!}{%
  \begin{tabular}{ r | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm} | p{2cm}  }
    \hline
    \multirow{2}{*}{Optical Flow} & \multicolumn{2}{c|}{Ego-Motion Converter} & \multicolumn{2}{c|}{Pixel-Motion Converter} &   \multirow{2}{*}{KITTI-Noc} &   \multirow{2}{*}{KITTI-All} \\ \hhline{~----~~}
    &100ms Task & 300ms Task & 100ms Task & 300ms Task &&\\
    \hline
    DeepFlow~\cite{weinzaepfel2013deepflow} & 18.60\% & 20.29\% & 9.71\% & 25.14\% & 7.22\%  &  14.57\%  \\ 
    classic++~\cite{spangenberg2014large} &25.14\%& 24.66\% & 10.13\% & 27.35\% & 10.49\% & 20.64\%\\
    SPS-FL~\cite{bradski2000opencv} &28.60\%& 28.68\% & 12.17\% & 34.60\% & 3.38\% & 10.06\% \\
     \hline
  \end{tabular}}
\end{center} 
\caption{3-pixel error rate of flow algorithms using different conversions and their error rate in KITTI.}
\label{tb:flow}
\end{table*}

\noindent\textbf{Analysis of Stereo Evaluation Result.}  Comparing with KITTI, GCS and S+GF have lower scores in our evaluation, indicating they are vulnerable to ambiguous and saturated regions. Opencv-BM gains in performance after combining with slanted plane converter, showing we can improved the result with smoothing. Fig.~\ref{fig:best_worst} shows examples of images with the most and least errors --- scenes that are wide-open are easy for stereo algorithms while they are less accurate when there are objects near the camera. 
\begin{figure}
\centering
\begin{subfigure}[b]{0.45\textwidth}
 \includegraphics[width=\textwidth]{img/img6/000168_raytrace_2.png}
                \subcaption{\small 3-pixel error rate: 26.57\%}
\end{subfigure}

\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{img/img6/000527_raytrace_2.png}
                 \subcaption{\small 3-pixel error rate: 1.33\%}
\end{subfigure}
\caption{The best and worst performed images of SPS-ST. Images are converted to 3D planes and visualized with lidar points mapped on top. The color of lidar points shows whether the algorithm correctly predicts the flight time of the lidar ray, where red means ``correct predict'' and blue means ``wrong predict''.}
\label{fig:best_worst}
\end{figure}

\noindent\textbf{Optical Flow Analysis.} Contrary to KITTI, DeepFlow and Classic++ perform better than SPS-FL in Tval. Analysis shows that this is because both DeepFlow and Classic++ can handle moving objects that are common in our dataset, while SPS-FL relies on the assumption that the scene is static. Fig.~\ref{fig:flow_error} visualizes their difference in performance. 

\begin{figure}
\centering
\begin{subfigure}[b]{0.35\textwidth}
 				\includegraphics[width=\textwidth]{img/flow/c3.pdf}
                \subcaption{\small Classic++ error: 9.13\%}
\end{subfigure}
\begin{subfigure}[b]{0.35\textwidth}
 				\includegraphics[width=\textwidth]{img/flow/y3.pdf}
                 \subcaption{\small SPS-FL error: 25.07\%}
\end{subfigure}

%\end{center} 
\caption{Above: a test case of optical flow with a car moving forward relative to the camera. Wrong predictions are visualized in colors green and blue: green points are where the actual pulse has longer flight time than prediction, while the blue points are where actual lidar flight time is shorter. We can see that Classic++ successfully captures the forward motion of the car where SPS-FL fails. Below: the 3-pixel error rate of optical flow algorithms. The depth for the optical flow converter is based on the result of SPS-ST.}
\label{fig:flow_error}
\end{figure}


\noindent\textbf{Conversion Method Analysis.} For scene reconstruction, slanted plane converters perform better then Delaunay triangulation, as the results are smoothed with slanted planes. In the 300ms flow task, the ego-motion converter performs better while in 100ms flow task, pixel-motion converter usually gives better accuracy. This is because in 300ms flow task, inaccurate depth estimation will have severe impact on lidar prediction, as the error will be accumulated over the frames.

\noindent\textbf{Relationship with KITTI.} In the case of stereo, the lidar prediction error is comparable with the 3-pixel error used in KITTI. Note that our dataset is more challenging since KITTI manually removes ambiguous regions whereas we score on all valid lidar rays after. For flow, we observe more severe error than KITTI. This could be expected as we are solving a harder problem --- predicting the scene in the future. Moreover, both the error in flow and depth estimation results in discrepancy of future lidar prediction.

\noindent\textbf{Result Visualization.} We visualize the evaluation result in both 3D and 2D. Our 3D visualization (Fig. ~\ref{fig:best_worst}) visualizes the 3D point in space overlayed with slanted planes or meshes, while our 2D visualization projects the points to image using default calibrations (Fig.~\ref{fig:flow_error}). 

Fig \ref{fig:more} shows more evaluation results.

\section{Conclusion and Future Work}

We define new performance measure --- lidar prediction --- to evaluate 3D vision algorithms. We also propose a new methodology to evaluate visual system on the basis of individual modules. Our experiment show that it can effectively evaluate stereo and optical flow in dynamic environments. 

Not limited to stereo or optical flow, Tval can be used as a general framework that can evaluate any 3D reconstruction ---- as long as it can predict the distance given lidar rays. We are interested in evaluating scene flow algorithm such as~\cite{rabe2010dense, vogel2014view, wedel2008efficient} as it computes depth and motion in 3D, and does not need default depth estimation as input. Another possible direction for our work is unsupervised learning of visual system --- we believe it is of great importance to be able to train visual system directly using lidar data and our performance measure. 
%%%%%%%%% BODY TEXT

\begin{figure*}[!ht]
\setlength{\abovecaptionskip}{1pt}
\setlength{\belowcaptionskip}{2pt}
\begin{center}
  \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/SPS/000060.png}
   \caption*{\small $4.4\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/SPS/000528.png}
   \caption*{\small $2.8\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/SPS/000731.png}
   \caption*{\small $33.1\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/SPS/000737.png}
   \caption*{\small $15.3\%$}
 \end{minipage}

  \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/GCS/000060.png}
   \caption*{\small $15.0\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/GCS/000528.png}
   \caption*{\small $21.2\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/GCS/000731.png}
   \caption*{\small $44.0\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/stereo/GCS/000737.png}
   \caption*{\small $24.3\%$}
 \end{minipage}

  \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/yamaguchi2/000004.png}
   \caption*{\small $21.8\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/yamaguchi2/000009.png}
   \caption*{\small $14.3\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/yamaguchi2/000014.png}
   \caption*{\small $19.1\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/yamaguchi2/000089.png}
   \caption*{\small $25.3\%$}
 \end{minipage}

  \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/deep/000004.png}
   \caption*{\small $18.4\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/deep/000009.png}
   \caption*{\small $13.0\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/deep/000014.png}
   \caption*{\small $22.1\%$}
 \end{minipage}
 \begin{minipage}[t]{0.23\textwidth}
   \centering
   \includegraphics[width=\textwidth]{img/summary/flow/deep/000089.png}
   \caption*{\small $10.6\%$}
 \end{minipage}

 \caption{Stereo and flow result visualization. \textbf{First row:} stereo result of SPS-St. \textbf{Second row:} stereo result of GCS. \textbf{Third row:} flow result of SPS-FL. \textbf{Fourth row:} flow result of DeepFlow. The number below each image is the 3-pixel error rate.}
\label{fig:more}
\end{center} 
\end{figure*}

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}


\end{document}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
